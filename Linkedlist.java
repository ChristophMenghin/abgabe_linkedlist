
public class Linkedlist {
	
	public class Node{
		private Node next;
		private Node previous;
		private int i;
		
		public Node(Node next,Node previous, int i){
			this.next = next;
			this.previous = previous;
			this.i = i;
			
			
		}

		public Node getPrevious() {
			return previous;
		}

		public void setPrevious(Node previous) {
			this.previous = previous;
		}

		public Node getNext() {
			return next;
		}

		public void setNext(Node next) {
			this.next = next;
		}

		public int getI() {
			return i;
		}

		public void setI(int i) {
			this.i = i;
		}
	}
	Node head = null;
	
	public static void main(String[] args)
	{
		new Linkedlist();
	}
	public Linkedlist(){
		addElement(5);
		addElement(10);
		addElement(15);
		
		Node n = head;
		while (n.next != null)
		{
			System.out.println(n.getI());
			n = n.getNext();
		}
		System.out.println(n.getI());
		
		while (n.previous != null)
		{
			System.out.println(n.getI());
			n = n.getPrevious();
		}
		System.out.println(n.getI());
	}
	private void addElement(int i)
	{
		if (head == null)
		{
			head = new Node(null,null, i);
		}else
		{
			Node n = head;
			while (n.next != null)
			{
				n = n.getNext();
			}
			Node node123 = new Node(null, null, i);
			n.setNext(node123);
		    node123.setPrevious(n);	
		}
		
		
	}
}